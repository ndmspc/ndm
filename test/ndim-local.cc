#include "Utils.hh"
#include "Space.hh"
#include "Config.hh"

int main(int /*argc*/, char const ** /*argv*/)
{

    int rc = 0;

    std::string configFile = "etc/default.yaml";
    if (getenv("NDM_CONFIG")) configFile = getenv("NDM_CONFIG");

    spdlog::info("Using config file [{}] ...", configFile);
    NDM::Config cfg;
    if (!cfg.load(configFile)) {
        spdlog::critical("Problem loading config file [{}] !!!", configFile);
        return ++rc;
    }

    std::string base = cfg.base();
    if (getenv("NDM_SPACE_BASE")) base = getenv("NDM_SPACE_BASE");
    if (!base.empty() && base.back() != '/') base += "/";
    if (!base.empty()) {
        struct stat st;
        if (!(stat(base.data(), &st) == 0 && S_ISDIR(st.st_mode))) {
            spdlog::warn("Base directory '{}' does not exist. Creating it ...", base);
            NDM::Utils::mkpath(base.data(), S_IRWXU);
            if (!(stat(base.data(), &st) == 0 && S_ISDIR(st.st_mode))) {
                spdlog::error("Could not create directory '{}' !!! Aborting ...", base);
                return -1;
            }
            spdlog::info(" Directory '{}' was created.", base);
        }
    }
    std::string  serialize, strasci, dir, cmd = cfg.cmd();
    std::string  pointsList = base;
    NDM::Space * s          = cfg.space();
    if (s == nullptr) {
        spdlog::critical("Problem loading Space object from config file [{}] !!!", configFile);
        return ++rc;
    }
    if (s->axes().size() <= 0) {
        spdlog::error("No axis was found ...", base);
        return ++rc;
    }
    // NDM::Axis a(0, 128);
    // a.info("1"); // setting IDaxis=1 for now
    // s->add(a);
    // std::vector<int>          levels = {2};

    std::vector<NDM::Point> points;
    s->points(cfg.levels(), points, 0);

    pointsList += "points";
    for (auto l : cfg.levels()) {
        pointsList += "_" + std::to_string(l);
    }
    pointsList += ".ndm";
    std::ofstream pointsListStream(pointsList.data());
    if (pointsListStream.fail()) {
        spdlog::error("Could not create {} in {} directory. Exiting.", pointsList, base);
        return -1;
    }

    if (points.size() == 0) return ++rc;

    spdlog::info("Path to directories: ");
    for (auto p : points) {
        dir.clear();
        dir = base + p.path();
        spdlog::info("{}", dir);
        NDM::Utils::mkpath(dir.data(), S_IRWXU);
        dir += "run.sh";
        std::ofstream out(dir);
        chmod(dir.data(), 0755);
        if (out.fail()) {
            spdlog::error("Could not create run.sh in {} directory. Exiting.", dir);
            return -1;
        }
        p.SerializeToString(&serialize);
        strasci.clear();
        NDM::Utils::protobuf_to_asci(serialize, strasci);

        out << "#!/bin/bash" << std::endl;
        out << "export NDMPOINT=" << strasci << std::endl;
        out << cmd << std::endl;
        pointsListStream << dir << std::endl;
        out.close();
    }
    pointsListStream.close();

    return rc;
}