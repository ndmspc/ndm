#include <getopt.h>
#include "ndm.hh"
#include "Utils.hh"
#include "Space.hh"
#include "Config.hh"

[[noreturn]] void version()
{
    spdlog::set_pattern("%v");
    spdlog::info("{} v{}.{}.{}-{}", ndm_NAME, ndm_VERSION_MAJOR(ndm_VERSION), ndm_VERSION_MINOR(ndm_VERSION),
                 ndm_VERSION_PATCH(ndm_VERSION), ndm_VERSION_TWEAK);
    spdlog::drop_all();
    exit(0);
}

[[noreturn]] void help()
{

    spdlog::set_pattern("%v");
    spdlog::info("{} v{}.{}.{}-{}", ndm_NAME, ndm_VERSION_MAJOR(ndm_VERSION), ndm_VERSION_MINOR(ndm_VERSION),
                 ndm_VERSION_PATCH(ndm_VERSION), ndm_VERSION_TWEAK);
    spdlog::info("");
    spdlog::info("Usage: {} [prog] [OPTION]...", ndm_NAME);
    spdlog::info("");
    spdlog::info("Options:");
    spdlog::info("       init               Init project");
    spdlog::info("       info               Info");
    spdlog::info("           --silent             no logs printed");
    spdlog::info("           --debug              debug logs printed");
    spdlog::info("           --trace              trace logs printed");
    spdlog::info("");
    spdlog::info("       -h, --help               display this help and exit");
    spdlog::info("       -v, --version            output version information and exit");
    spdlog::info("");
    spdlog::info("Examples:");
    spdlog::info("       ndm init");
    spdlog::info("                                Intialize project dir");
    spdlog::info("");
    spdlog::info("       ndm info");
    spdlog::info("                                Prints statistics of cuurrent point");
    spdlog::info("");
    spdlog::info("Report bugs to: martin.vala@cern.ch");
    spdlog::info("Pkg home page: <https://gitlab.com/ndmspc/ndm>");
    spdlog::info("General help using GNU software: <https://www.gnu.org/gethelp/>");

    spdlog::drop_all();

    exit(0);
}

int main(int argc, char * const * argv)
{

    int rc = 0;
    // 'info' level (-1 means not setting log level)
    int debugLevel = static_cast<int>(spdlog::level::info);
    if (getenv("NDM_DEBUG_LEVEL")) debugLevel = atoi(getenv("NDM_DEBUG_LEVEL"));
    std::string point_output_filename;
    // ***** Default values END *****
    std::string cmd;

    if (argc > 1) cmd = argv[1];

    std::string   shortOpts  = "hvo:W;";
    struct option longOpts[] = {{"help", no_argument, nullptr, 'h'},
                                {"version", no_argument, nullptr, 'v'},
                                {"output", required_argument, nullptr, 'o'},
                                {"trace", no_argument, &debugLevel, static_cast<int>(spdlog::level::trace)},
                                {"debug", no_argument, &debugLevel, static_cast<int>(spdlog::level::debug)},
                                {"silent", no_argument, &debugLevel, static_cast<int>(spdlog::level::off)},
                                {nullptr, 0, nullptr, 0}};

    int nextOption = 0;
    do {
        nextOption = getopt_long(argc, argv, shortOpts.c_str(), longOpts, nullptr);
        switch (nextOption) {
        case -1:
        case 0: break;
        case 'o': point_output_filename = optarg; break;
        case 'h': help();
        case 'v': version(); break;
        default: help();
        }
    } while (nextOption != -1);

    spdlog::set_level(static_cast<spdlog::level::level_enum>(debugLevel));

    spdlog::info("{} v{}.{}.{}-{}", ndm_NAME, ndm_VERSION_MAJOR(ndm_VERSION), ndm_VERSION_MINOR(ndm_VERSION),
                 ndm_VERSION_PATCH(ndm_VERSION), ndm_VERSION_TWEAK);
    std::string pwd = NDM::Utils::get_working_path();

    if (cmd.empty()) return 2;

    if (cmd == "init") {
        NDM::Utils::ndm_init(pwd);

    }
    else if (cmd == "info") {
        if (NDM::Utils::ndm_info(point_output_filename)) return 10;
    } else {
        spdlog::error("Unknown command '{}' !!!",cmd);
        spdlog::error("  Suppoted commands: init info");
    }
    return rc;
}