#include <chrono>

#include "../src/Space.hh"
#include "gtest/gtest.h"

namespace NDM {

// The fixture for testing class Space.
class SpaceTest : public ::testing::Test {
protected:
    // You can remove any or all of the following functions if their bodies would
    // be empty.

    SpaceTest()
    {
        // You can do set-up work for each test here.
    }

    ~SpaceTest() override
    {
        // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    void SetUp() override
    {
        // Code here will be called immediately after the constructor (right
        // before each test).
    }

    void TearDown() override
    {
        // Code here will be called immediately after each test (right
        // before the destructor).
    }

    // Class members declared here can be used by all tests in the test suite
    // for Space.
};

TEST_F(SpaceTest, Default)
{
    Space s;
    EXPECT_EQ(s.axes().size(), 0);
}

TEST_F(SpaceTest, size)
{
    Space s;
    Axis  a(1, 10);
    Axis  b(0, 1);
    s.add(a);
    s.add(b);
    EXPECT_EQ(s.axes().size(), 2);
}

TEST_F(SpaceTest, axis)
{
    Space s;
    Axis  a(1, 10);
    Axis  b(0, 1);
    s.add(a);
    s.add(b);

    EXPECT_EQ(s.axis(0).min(), 1);
    EXPECT_EQ(s.axis(0).max(), 10);
    EXPECT_EQ(s.axis(1).min(), 0);
    EXPECT_EQ(s.axis(1).max(), 1);
}
TEST_F(SpaceTest, CheckRunLevelsAndAxis1D)
{
    Space s;
    Axis  a(0, 128);
    a.info("1");
    s.add(a);
    std::vector<int>        levels = {2};
    std::vector<NDM::Point> points;
    s.points(levels, points, 0);
    EXPECT_EQ(points.size(), 4);
}

TEST_F(SpaceTest, CheckSplitLevels)
{
    Space s;
    Axis  a(-10, 10, NDM::Axis::kAligned);
    s.add(a);
    std::vector<int>        levels = {4};
    std::vector<NDM::Point> points;
    s.points(levels, points, 0);
    EXPECT_EQ(points.size(), 10);
}

TEST_F(SpaceTest, CheckRunLevelsAndAxisnD)
{
    Space s;
    Axis  a(0, 128);
    Axis  b(0, 64);

    a.info("1");
    b.info("1");
    s.add(a);
    s.add(b);
    std::vector<int>        levels = {3, 2};
    std::vector<NDM::Point> points;
    s.points(levels, points, 0);
    EXPECT_EQ(points.size(), 32);
}
TEST_F(SpaceTest, CheckExecutionTimeOfPoints)
{
    Space s;
    Axis  a(0, 128);
    Axis  b(0, 128);
    Axis  c(0, 128);
    Axis  d(0, 128);
    Axis  e(0, 128);
    Axis  f(0, 128);

    a.info("1");
    b.info("1");
    c.info("1");
    d.info("1");
    e.info("1");
    f.info("1");

    s.add(a);
    s.add(b);
    s.add(c);
    s.add(d);
    s.add(e);
    s.add(f);

    std::vector<int>        levels = {3, 3, 3, 3, 3, 3};
    std::vector<NDM::Point> points;
    auto                    start = std::chrono::high_resolution_clock::now();

    s.points(levels, points, 0);
    auto stop     = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop - start);
    spdlog::info("Time taken for execution of function points= {}", duration.count());
    // EXPECT_EQ(points.size(), 32);
}

TEST_F(SpaceTest, FindPoint)
{
    Space s;
    Axis  a(0, 128);
    Axis  b(0, 128);

    a.info("1");
    b.info("1");
    s.add(a);
    s.add(b);
    std::vector<int> levels     = {3, 3};
    std::vector<int> coordinate = {3, 1};
    NDM::Point       point;
    s.find_point(coordinate, levels, point);
    point.PrintDebugString();
}
int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
} // namespace NDM
