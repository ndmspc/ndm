#include <vector>
#include "ndm.hh"
#include "Space.hh"
int main(int /*argc*/, char const ** /*argv*/)
{
    NDM::Space s;

    double      min, max;
    std::string path;
    NDM::Axis * a;
    a = new NDM::Axis(0, 8);
    a->print();
    a->find(4, min, max, path);
    spdlog::info("v=4 <{},{}) path={}", min, max, path);
    a->expand('>', 1);
    a->print();
    a->expand('<', 1);
    a->print();
    s.add(*a);

    a->shrink('<', 1);
    a->shrink('>', 1);
    a->print();

    a->shrink('>', 3);
    a->print();

    a->shrink('>', 1);
    s.add(*a);
    delete a;

    path.clear();

    a = new NDM::Axis(-0.5, 0.5);
    a->print();
    a->find(0.3, min, max, path);
    spdlog::info("v=0.3 <{},{}) path={}", min, max, path);
    path.clear();
    a->find(-0.3, min, max, path);
    spdlog::info("v=-0.3 <{},{}) path={}", min, max, path);
    path.clear();

    s.add(*a);
    delete a;

    a = new NDM::Axis(-0.5, 0.5, NDM::Axis::kFixed, 0.1);
    a->print();
    a->find(0.3, min, max, path);
    spdlog::info("v=0.3 <{},{}) path={}", min, max, path);
    path.clear();

    s.add(*a);
    delete a;
    spdlog::info("Printing space {}", s.axes().size());
    s.print();

    return 0;
}
