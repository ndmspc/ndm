#include <vector>
#include <fstream>
#include <sstream>
#include <bitset>
#include "ndm.hh"
#include "Utils.hh"
#include "Axis.hh"
#include "Point.pb.h"
int main(int /*argc*/, char const ** /*argv*/)
{

    spdlog::info("Creating point ...");
    NDM::Point p;
    p.set_path("0/0/");

    NDM::Coordinate * c = p.add_coordinates();
    c->set_min(55);
    c->set_max(100);
    c->set_info("1");

    c = p.add_coordinates();
    c->set_min(-10);
    c->set_max(10);
    c->set_info("2");

    p.PrintDebugString();
    std::string strorig;
    p.SerializeToString(&strorig);

    spdlog::info("Testing env support ...");

    spdlog::info("strorig={}", strorig);
    std::string strasci;
    NDM::Utils::protobuf_to_asci(strorig,strasci);

    setenv("NDM_POINT", strasci.data(), 1);
    spdlog::info("export NDM_POINT={}",strasci.data());
    std::string env_point = getenv("NDM_POINT");

    NDM::Point p_new;
    std::string strasci2;
    NDM::Utils::asci_to_protobuf(env_point,strasci2);
    bool       b = p_new.ParseFromString(strasci2);
    spdlog::info("rc={}", b);
    p_new.PrintDebugString();

    spdlog::info("Testing file support ...");

    std::ofstream ofs("/tmp/ndmpoint.txt", std::ios_base::out | std::ios_base::binary);
    p.SerializeToOstream(&ofs);
    ofs.close();

    std::ifstream ifs("/tmp/ndmpoint.txt", std::ios_base::in | std::ios_base::binary);
    b = p_new.ParseFromIstream(&ifs);
    spdlog::info("rc={}", b);
    p_new.PrintDebugString();
    ifs.close();

    return 0;
}
