#include "../src/Axis.hh"

#include "gtest/gtest.h"

namespace NDM {

// The fixture for testing class Axis.
class AxisTest : public ::testing::Test {
protected:
    // You can remove any or all of the following functions if their bodies would
    // be empty.

    AxisTest()
    {
        // You can do set-up work for each test here.
    }

    ~AxisTest() override
    {
        // You can do clean-up work that doesn't throw exceptions here.
    }

    // If the constructor and destructor are not enough for setting up
    // and cleaning up each test, you can define the following methods:

    void SetUp() override
    {
        // Code here will be called immediately after the constructor (right
        // before each test).
    }

    void TearDown() override
    {
        // Code here will be called immediately after each test (right
        // before the destructor).
    }

    // Class members declared here can be used by all tests in the test suite
    // for Axis.
};

// Tests that the Axis::Bar() method does Abc.
TEST_F(AxisTest, Default)
{
    Axis a;
    EXPECT_EQ(a.min(), 0);
    EXPECT_EQ(a.max(), 1);
    EXPECT_EQ(a.maxb_user(), 1);
    EXPECT_EQ(a.maxb(), 1);
}

TEST_F(AxisTest, RangeBellowZero)
{
    Axis a(0.25, 0.5);
    EXPECT_EQ(a.min(), 0.25);
    EXPECT_EQ(a.max(), 0.5);
    EXPECT_EQ(a.maxb_user(), 25);
    EXPECT_EQ(a.maxb(), 32);
}

TEST_F(AxisTest, RangeBellowOverZero)
{
    Axis a(0.25, 1.3);
    EXPECT_EQ(a.min(), 0.25);
    EXPECT_EQ(a.max(), 1.3);
    EXPECT_EQ(a.maxb_user(), 105);
    EXPECT_EQ(a.maxb(), 128);
}

TEST_F(AxisTest, RangeOverZero)
{
    Axis a(3, 6);
    EXPECT_EQ(a.min(), 3);
    EXPECT_EQ(a.max(), 6);
    EXPECT_EQ(a.maxb_user(), 3);
    EXPECT_EQ(a.maxb(), 4);
}

TEST_F(AxisTest, RangeBigNumbers)
{
    Axis a(1000, 2000);
    EXPECT_EQ(a.min(), 1000);
    EXPECT_EQ(a.max(), 2000);
    EXPECT_EQ(a.maxb_user(), 1);
    EXPECT_EQ(a.maxb(), 1);
}

TEST_F(AxisTest, RangeBigNumbersWithThreeDesimalsAfterDotFromZero)
{
    Axis a(0, 10.345);
    EXPECT_EQ(a.min(), 0);
    EXPECT_EQ(a.max(), 10.345);
    EXPECT_EQ(a.maxb_user(), 10345);
    EXPECT_EQ(a.maxb(), 16384);
}

TEST_F(AxisTest, RangeBigNumbersWithThreeDesimalsAfterDot)
{
    Axis a(2.53, 10.345);
    EXPECT_EQ(a.min(), 2.53);
    EXPECT_EQ(a.max(), 10.345);
    EXPECT_EQ(a.maxb_user(), 7815);
    EXPECT_EQ(a.maxb(), 8192);
}

TEST_F(AxisTest, MinMaxMissmatch)
{
    Axis a(5, 2);
    EXPECT_EQ(a.min(), 2);
    EXPECT_EQ(a.max(), 5);
    EXPECT_EQ(a.maxb_user(), 3);
    EXPECT_EQ(a.maxb(), 4);
}

TEST_F(AxisTest, AlignedMode)
{
    Axis a(2, 5);
    EXPECT_EQ(a.min(), 2);
    EXPECT_EQ(a.max(), 5);
    EXPECT_EQ(a.maxb_user(), 3);
    EXPECT_EQ(a.maxb(), 4);
}
TEST_F(AxisTest, FixedMode)
{
    Axis a(2, 5, NDM::Axis::kFixed);
    EXPECT_EQ(a.min(), 2);
    EXPECT_EQ(a.max(), 5);
    EXPECT_EQ(a.maxb_user(), 3);
    EXPECT_EQ(a.maxb(), 3);
}

TEST_F(AxisTest, FindZeroEightInRange)
{

    double      min = 0;
    double      max = 8;
    std::string path;
    Axis        a(min, max);
    a.find(0, min, max, path);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 8);

    a.find(1, min, max, path);
    EXPECT_EQ(min, 1);
    EXPECT_EQ(max, 2);

    a.find(2, min, max, path);
    EXPECT_EQ(min, 2);
    EXPECT_EQ(max, 4);

    a.find(3, min, max, path);
    EXPECT_EQ(min, 3);
    EXPECT_EQ(max, 4);

    a.find(4, min, max, path);
    EXPECT_EQ(min, 4);
    EXPECT_EQ(max, 8);

    a.find(5, min, max, path);
    EXPECT_EQ(min, 5);
    EXPECT_EQ(max, 6);

    a.find(6, min, max, path);
    EXPECT_EQ(min, 6);
    EXPECT_EQ(max, 8);

    a.find(7, min, max, path);
    EXPECT_EQ(min, 7);
    EXPECT_EQ(max, 8);
}

TEST_F(AxisTest, FindZeroEightOutOfRange)
{

    double      min = 0;
    double      max = 8;
    std::string path;

    Axis a(min, max);
    a.find(-1, min, max, path);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 0);

    a.find(8, min, max, path);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 0);
}

TEST_F(AxisTest, FindMinusFivePlusFive)
{

    double      min = -5;
    double      max = 5;
    std::string path;

    Axis a(min, max);
    a.print();
    a.find(-5, min, max, path);
    EXPECT_EQ(min, -5);
    EXPECT_EQ(max, 5);

    a.find(0.0, min, max, path);
    EXPECT_EQ(min, 0.0);
    EXPECT_EQ(max, 1.0);
}

TEST_F(AxisTest, FindMinusFivePlusFiveDivTen)
{

    double      min = -0.5;
    double      max = 0.5;
    std::string path;

    Axis a(min, max);
    a.print();
    a.find(-0.5, min, max, path);
    EXPECT_EQ(min, -0.5);
    EXPECT_EQ(max, 0.5);

    a.find(0.0, min, max, path);
    EXPECT_EQ(min, 0.0);
    EXPECT_EQ(max, 0.1);
}

TEST_F(AxisTest, FindMinusFivePlusFiveDivHundred)
{

    double      min = -0.05;
    double      max = 0.05;
    std::string path;

    Axis a(min, max);
    a.print();
    a.find(-0.05, min, max, path);
    EXPECT_EQ(min, -0.05);
    EXPECT_EQ(max, 0.05);

    a.find(0.0, min, max, path);
    EXPECT_EQ(min, 0.00);
    EXPECT_EQ(max, 0.01);
}

TEST_F(AxisTest, FindMinusFivePlusFiveTimesTen)
{

    double      min = -50;
    double      max = 50;
    std::string path;

    Axis a(min, max);
    a.print();
    a.find(-50, min, max, path);
    EXPECT_EQ(min, -50);
    EXPECT_EQ(max, 50);

    a.find(0.0, min, max, path);
    EXPECT_EQ(min, 0.0);
    EXPECT_EQ(max, 2);
}

TEST_F(AxisTest, FindMinusFivePlusFiveTimesHundred)
{

    double      min = -500;
    double      max = 500;
    std::string path;

    Axis a(min, max);
    a.print();
    a.find(-500, min, max, path);
    EXPECT_EQ(min, -500);
    EXPECT_EQ(max, 500);

    a.find(0.0, min, max, path);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 4);
}

TEST_F(AxisTest, CheckLevelRequest)
{

    double      min = 0;
    double      max = 200;
    std::string path;

    Axis a(min, max, NDM::Axis::kFixed);
    a.print();
    a.find(0, min, max, path, 1);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 100);
    path.clear();

    a.find(0, min, max, path, 2);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 50);
    path.clear();

    a.find(0, min, max, path, 3);
    EXPECT_EQ(min, 0);
    EXPECT_EQ(max, 25);
    path.clear();

}

} // namespace NDM

int main(int argc, char ** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}