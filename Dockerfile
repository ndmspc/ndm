FROM registry.gitlab.com/ndmspc/ndm/dep/c8 AS builder
ARG MY_PROJECT_VER=0.0.0-rc0
COPY . /builder/
WORKDIR /builder
RUN scripts/make.sh clean rpm

FROM centos:8
COPY --from=builder /builder/build/RPMS/x86_64/*.rpm /
RUN rm ndm-devel*.rpm
RUN dnf update -y
RUN dnf install epel-release 'dnf-command(config-manager)' 'dnf-command(copr)' -y
RUN dnf config-manager --set-enabled powertools
RUN dnf copr enable ndmspc/stable -y
RUN dnf copr enable ndmspc/testing -y
RUN dnf install -y *.rpm
ENTRYPOINT [ "ndim-test" ]
