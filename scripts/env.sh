#!/bin/bash

PROJECT_DIR="$(dirname $(dirname $(readlink -m ${BASH_ARGV[0]})))"

export PATH="$PROJECT_DIR/bin:$PATH"
export NDM_INCLUDE_DIR="$PROJECT_DIR/include"
export NDM_LIB_DIR="$PROJECT_DIR/lib64"
export LD_LIBRARY_PATH="$NDM_LIB_DIR:$LD_LIBRARY_PATH"
export CPLUS_INCLUDE_PATH="$NDM_INCLUDE_DIR/ndm:$CPLUS_INCLUDE_PATH"
