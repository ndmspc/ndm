#include <iostream>
#include <cmath>

#include "ndm.hh"
#include "Utils.hh"

#include "Axis.hh"

namespace NDM {
Axis::Axis(double min, double max, EMode mode, double delta) : mMin(min), mMax(max), mMode(mode), mMinDelta(delta)
{
    ///
    /// Constructor
    ///

    // Chcek if min is higher then max. If it is switch min with max
    if (min > max) {
        spdlog::warn("Swaping min[{}] and max[{}] values entered by user, because min is higher then max !!!", min,
                     max);
        mMax = min;
        mMin = max;
    }

    recalculate_range();
}
Axis::~Axis()
{
    ///
    /// Destructor
    ///
}

void Axis::recalculate_range()
{
    ///
    /// Recalculate range correctly
    ///

    // Checking for numer of decimels after dot
    int decimalsMin = decimels_right(mMin);
    int decimalsMax = decimels_right(mMax);

    // If both decimels are zero then we check, if we can divide to lower number
    if (decimalsMin == 0 && decimalsMax == 0) {

        // Checking for numer of decimels before dot
        decimalsMin = decimels_left(mMin);
        decimalsMax = decimels_left(mMax);

        // Calculate decimel minimum
        int m = std::min(decimalsMin, decimalsMax);

        // Sets coresponding values for mMultiply and mShift
        mMultiply = 1 / std::pow(10, m);
        mShift    = mMin * mMultiply;
    }
    else {

        // Calculate decimel maximum
        int m = std::max(decimalsMin, decimalsMax);

        // Sets coresponding values for mMultiply and mShift
        mMultiply = std::pow(10, m);
        mShift    = mMin * mMultiply;
    }

    // Handle mode setting
    if (mMode == kAligned)
        mMaxB = std::pow(2, ceil(log2(maxb_user())));
    else
        mMaxB = maxb_user();
}

void Axis::minmax(double min, double max)
{
    ///
    /// Setting min and max
    ///
    mMin = min;
    mMax = max;
}

void Axis::find(double v, double & min, double & max, std::string & path, int levelrequest, int levelmax)
{
    ///
    /// Find bin and print info for given value with maximum depth 'levelmax'
    ///

    spdlog::trace("Finding value[{}] levelrequest[{}] levelmas[{}]...", v, levelrequest, levelmax);

    // Reseting path
    path = "";

    int currentlevel = 0;
    min              = 0;
    max              = maxb();

    if (v < mMin || v > mMax || NDM::Utils::is_equal(v, mMax)) {
        spdlog::warn("Value '{}' is out of range <{},{}) !!!", v, mMin, mMax);
        min = max = 0;
        return;
    }

    v = to_internal(v);

    find(v, levelrequest, levelmax, currentlevel, min, max, path);
    if (max > to_internal(mMax)) max = to_internal(mMax);

    // We should convert min and max to user format
    min = to_user(min);
    max = to_user(max);
    spdlog::trace("Result value[{}] <{},{}) path[{}] level[{}] internal[<{},{})] ...", to_user(v), min, max, path,
                  currentlevel, to_internal(min), to_internal(max));
}

void Axis::find(double & v, int & levelrequest, int & levelmax, int & currentlevel, double & min, double & max,
                std::string & path)
{
    ///
    /// Find bin and print info for given value with maximum depth 'levelmax'
    ///

    if (NDM::Utils::is_equal(v, min)) {
        if (currentlevel >= levelrequest) return;
    }
    if (NDM::Utils::is_equal(v, max)) {
        if (currentlevel == 0) return;
        double oldmax = max;
        max += (max - min);
        min = oldmax;
        return;
    }

    if ((max - min) < mMinDelta) return;

    if (currentlevel > levelmax) return;

    currentlevel++;

    double avg = (max + min) / 2;
    spdlog::trace("min[{}]({}) max[{}]({}) avg[{}]({}) value[{}] ...", min, to_user(min), max, to_user(max), avg,
                  to_user(avg), to_user(v));
    if (v >= avg) {
        min = avg;
        path += "1";
    }
    else {
        max = avg;
        path += "0";
    }

    spdlog::trace("level=[{}] path={}...", currentlevel, path);
    find(v, levelrequest, levelmax, currentlevel, min, max, path);
}

void Axis::expand(char direction, int power)
{
    ///
    /// Expand range by 2^power to left '<' or right '>' direction
    ///
    modify_range(direction, std::abs(power));
}

void Axis::shrink(char direction, int power)
{
    ///
    /// Shrink range by 2^power to left '<' or right '>' direction
    ///
    modify_range(direction, -std::abs(power));
}

void Axis::modify_range(char direction, int power)
{
    ///
    /// Modify (expand/shrink) range by 2^power ('+' for up or '-'for down) to left '<' or right '>' direction
    ///

    if (power == 0) {
        spdlog::warn("Power is zero !!! Doing nothing ...");
        return;
    }

    if ((power < 0) && std::pow(2, std::abs(power)) > mMaxB) {
        print();
        spdlog::warn("Divide is not possible !!! Reason : mMaxB[{}] < divide[{}]", mMaxB, std::pow(2, std::abs(power)));
        return;
    }

    if (direction == '>') {
        if (power > 0) {
            for (int i = 0; i < power; ++i) mMax += (mMax - mMin);
        }
        else if (power < 0) {

            for (int i = 0; i < std::abs(power); ++i) mMax -= (mMax - mMin) / 2;
        }
    }
    else if (direction == '<') {
        if (power > 0) {
            for (int i = 0; i < power; ++i) mMin -= (mMax - mMin);
        }
        else if (power < 0) {

            for (int i = 0; i < std::abs(power); ++i) mMin += (mMax - mMin) / 2;
        }
    }
    else {
        spdlog::error("Direction '{}' is not supported !!!");
        spdlog::error("  Hint: Try '>' for right or '<' for left.");
    }

    recalculate_range();
}

void Axis::print() const
{
    ///
    /// Print axis information
    ///

    spdlog::info("min[{}] max[{}] maxb[{}({})] mode[{}] level[{}] shift[{}] multiply[{}]", mMin, mMax, mMaxB, maxb_user(),
                 (int)mMode, mLevel, mShift, mMultiply);
}

int Axis::decimels_right(double num, double mult, double min, double max)
{
    ///
    /// Calculate number of decimels on rigth side of dot
    ///

    int    n = 0;
    double fractpart, intpart, tmp;
    fractpart = modf(num, &intpart);
    spdlog::trace("L1 : num[{}] tmp[{}] intpart[{}] fractpart[{}]", num, tmp, intpart, fractpart);
    while (fractpart > min && fractpart < max) {
        tmp       = fractpart * mult;
        fractpart = modf(tmp, &intpart);
        spdlog::trace("L2 : num[{}] tmp[{}] intpart[{}] fractpart[{}]", num, tmp, intpart, fractpart);
        n++;
    }

    return n;
}

int Axis::decimels_left(double num, double mult, double min)
{
    ///
    /// Calculate number of decimels on left side of dot
    ///

    if (num < 0.01) return 0;

    int    n = 0;
    double fractpart, intpart, tmp;
    fractpart = modf(num, &intpart);
    spdlog::trace("H1 : num[{}] tmp[{}] intpart[{}] fractpart[{}]", num, tmp, intpart, fractpart);
    while (fractpart < min) {
        tmp       = intpart / mult;
        fractpart = modf(tmp, &intpart);
        spdlog::trace("H2 : num[{}] tmp[{}] intpart[{}] fractpart[{}]", num, tmp, intpart, fractpart);
        n++;
    }
    return n - 1;
}

void Axis::split(std::vector<double> & mins, int level)
{
    ///
    /// Returns list of minimums for specific level
    ///
    mins.clear();
    double p2 = pow(2, level);
    if (p2 > mMaxB) {
        spdlog::error("Cannot split histogram with {} bins into {} levels because (2^{}={}) > {}. Stop.", mMaxB, level,
                      level, p2, mMaxB);
        return;
    }
    double step = mMaxB / p2;

    spdlog::debug("split: level[{}] max[{}] step[{}]", level, mMaxB, step);

    for (double iMins = 0; iMins < mMaxB; iMins += step) {
        if (iMins >= to_internal(mMax)) break;
        mins.push_back(to_user(iMins));
    }
}

} // namespace NDM