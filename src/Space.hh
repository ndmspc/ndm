#pragma once

#include <vector>
#include "ndm.hh"
#include "Axis.hh"
#include "Point.pb.h"
namespace NDM {

///
/// \class Space
///
/// \brief Space object in n-dimensional space
/// \author Martin Vala <vala.martin@gmail.com>
/// \author Ishaan Ahuja <ishaan.ahuja0@gmail.com>
///

class Space {

public:
    /// Default Constructor
    Space();
    /// Default Destructor
    virtual ~Space();

    void print() const;
    void add(NDM::Axis a);
    void points(std::vector<int> levels, std::vector<NDM::Point> & point, int idAxis = 0);
    void find_point(std::vector<int> & coordinates, std::vector<int> & levels, NDM::Point & point);

    Axis & axis(int id);
    /// Default space
    std::vector<NDM::Axis> & axes() { return mAxes; }
    std::string              get_full_path(std::vector<std::string> & paths);

private:
    std::vector<NDM::Axis>           mAxes{};   ///< Vector of axis to be used for space
    std::vector<std::vector<double>> mTmpMins;  ///< Temporary vector storing value of minimums of axes
    Point                            mTmpPoint; ///< Temporary Point Object
    std::vector<std::string>         mTmpPaths; ///< Temporary vector storing generated paths for points
};
} // namespace NDM