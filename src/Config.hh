#pragma once

#include <yaml.h>

namespace NDM {
///
/// \class Config
///
/// \brief Base Config class
/// \author Martin Vala <mvala@cern.ch>
/// \author Branislav Beke <bbeke@badhaven.dev>
///

class Space;
class Config {
public:
    Config();
    virtual ~Config();

    virtual bool load(std::string file);
    virtual void print() const;

    /// Returns base
    virtual std::string base() const { return mConfig["base"].as<std::string>(); }
    /// Returns cmd
    virtual std::string cmd() const { return mConfig["cmd"].as<std::string>(); }
    /// Returns space object
    virtual Space * space() const { return mSpace; }
    /// Returns list of levels
    virtual std::vector<int> levels() const { return std::move(mLevels); }
    /// Returns list of levels
    virtual std::vector<std::string> envs() const { return std::move(mEnvs); }

protected:
    YAML::Node               mConfig;         ///< YAML Configuration
    Space *                  mSpace{nullptr}; ///< Space object
    std::vector<int>         mLevels{};       ///< Levels for each axis
    std::vector<std::string> mEnvs{};         ///< List of env variables
};
} // namespace NDM
