#pragma once
#include <string>
#include <vector>
namespace NDM {
///
/// \class Axis
///
/// \brief Axis object in n-dimensional space
/// \author Martin Vala <vala.martin@gmail.com>
///

class Axis {
public:
    /// Mode for axis
    enum EMode { kAligned = 0, kFixed = 1 };

    Axis(double min = 0.0, double max = 1.0, EMode mode = kAligned, double delta = 1);
    virtual ~Axis();

    /// Prints axis info
    void print() const;
    /// Find bin and print info for given value
    void find(double v, double & min, double & max, std::string & path, int levelrequest = -1, int levelmax = 100);
    /// Expand range
    void expand(char direction = '>', int power = 1);
    /// Shrink range
    void shrink(char direction = '>', int power = 1);

    /// Sets user defined minimum
    void min(double m) { mMin = m; }
    /// Sets user defined maximum
    void max(double m) { mMax = m; }
    /// Sets user defined maximum
    void is_bin(bool b) { mIsBin = b; }
    /// Sets user defined minimum and maximum
    void minmax(double min, double max);
    /// Sets level
    void level(unsigned int l) { mLevel = l; }

    /// Returns user defined minimum
    double min() const { return mMin; }
    /// Returns user defined maximum
    double max() const { return mMax; }
    /// Returns user defined maximum
    bool is_bin() const { return mIsBin; }
    /// Returns internal maximum
    double maxb() const { return mMaxB; }
    /// Returns internal maximum2
    double maxb_user() const { return (mMax * mMultiply - mShift); }

    /// Sets info string
    void info(std::string i) { mInfo = i; }
    /// Returns info string
    std::string info() const { return mInfo; }

    /// Convert interal to user format
    double to_user(double v) const { return (v + mShift) / mMultiply; }
    /// Convert user to internal format
    double to_internal(double v) const { return (v * mMultiply) - mShift; }

    /// Returns level
    unsigned int level() const { return mLevel; }
    void         split(std::vector<double> & mins, int level);

private:
    double       mMin{0.0};       ///< User defined minimum
    double       mMax{1.0};       ///< User defined maximum
    bool         mIsBin{false};   ///< Min max is in bin/user format
    EMode        mMode{kAligned}; ///< Axis mode
    double       mMaxB{1};        ///< Internal  maximum
    double       mShift{0};       ///< Shift to the internal min/max parameters
    double       mMultiply{1};    ///< Multiply to the internal min/max parameters
    unsigned int mLevel{0};       ///< Level of division
    double       mMinDelta{1};    ///< Minimal delta of axis range
    std::string  mInfo{};         ///< Some additional info for axis

    int  decimels_right(double num, double mult = 10, double min = 0.01, double max = 0.99);
    int  decimels_left(double num, double mult = 10, double min = 0.01);
    void find(double & v, int & levelrequest, int & levelmax, int & currentlevel, double & min, double & max,
              std::string & path);
    void modify_range(char direction = '>', int power = 1);
    void recalculate_range();
};
} // namespace NDM