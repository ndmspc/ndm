#pragma once
#include <sys/stat.h>
#include <libgen.h>
#include <cmath>
#include <limits>
#include <fstream>
#include <string>
#include <sstream>
#include <bitset>
#include <google/protobuf/util/json_util.h>
#include "Point.pb.h"
#include "ndm.hh"

namespace NDM {
namespace Utils {

inline bool is_equal(double a, double b, double error_factor = 1.0)
{
    return a == b ||
           std::abs(a - b) < std::abs(std::fmin(a, b)) * std::numeric_limits<double>::epsilon() * error_factor;
}

inline void asci_to_protobuf(std::string a, std::string & p)
{
    std::stringstream sstream(a);
    p.clear();
    while (sstream.good()) {
        std::bitset<8> bits;
        sstream >> bits;
        char c = char(bits.to_ulong());
        p += c;
    }
}

inline void protobuf_to_asci(std::string p, std::string & a)
{
    for (std::size_t i = 0; i < p.size(); ++i) {
        std::bitset<8> xx = std::bitset<8>(p[i]);
        a += xx.to_string();
    }
}
inline int mkpath(const char * dir, mode_t mode)
{
    struct stat st;
    if (!dir) {
        errno = EINVAL;
        return 1;
    }
    if (!stat(dir, &st)) return 0;
    mkpath(dirname(strdupa(dir)), mode);
    return mkdir(dir, mode);
}

inline std::string get_working_path()
{
    char temp[128];
    return (getcwd(temp, sizeof(temp)) ? std::string(temp) : std::string(""));
}

inline bool ndm_init(std::string dir = "/tmp/")
{
    std::string ndm_yaml;
    ndm_yaml.append("base: \"" + dir + "/content\"\n");
    ndm_yaml.append("cmd: \"" + dir + "/ndm.sh\"\n");
    ndm_yaml.append("space:\n");
    ndm_yaml.append("  axes:\n");
    ndm_yaml.append("    - name: \"axis1\"\n");
    ndm_yaml.append("      min: 0\n");
    ndm_yaml.append("      max: 1\n");
    ndm_yaml.append("      level: 0\n");
    std::ofstream ndm_yaml_out("ndm.yaml");
    ndm_yaml_out << ndm_yaml;
    ndm_yaml_out.close();

    std::string ndm_script;
    ndm_script.append("#!/bin/bash\n");
    ndm_script.append("export WKDIR=\"$NDMBASE/$NDMPATH\"\n");
    ndm_script.append("mkdir -p $WKDIR && cd $WKDIR || exit 1\n");
    ndm_script.append("(\n");
    ndm_script.append("export\n");
    ndm_script.append("ndm info\n");
    ndm_script.append("# my job\n");
    ndm_script.append("sleep 1\n");
    ndm_script.append(") |& tee $WKDIR/ndm.log\n");
    std::ofstream ndm_script_out("ndm.sh");
    ndm_script_out << ndm_script;
    chmod("ndm.sh", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    ndm_script_out.close();

    std::string ndm_runscript;
    ndm_runscript.append("#!/bin/bash\n");
    ndm_runscript.append("SLS_URL=${1-\"tcp://toolbox.localhost:41000\"}\n\n");
    ndm_runscript.append("salsa-ndm -s $SLS_URL -c ndm.yaml\n");
    std::ofstream ndm_runscript_out("run.sh");
    ndm_runscript_out << ndm_runscript;
    chmod("run.sh", S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    ndm_runscript_out.close();

    spdlog::info("Files 'ndm.yaml', 'ndm.sh' and 'run.sh' generated");

    return true;
}

inline bool ndm_info(std::string out = "")
{
    const char* e = getenv("NDMPOINT");
    std::string ndmpoint;
    if (e) ndmpoint = e;

    if (ndmpoint.empty()) {
        spdlog::error("Missing env variable '$NDMPOINT' !!!");
        return false;
    }
    std::string ndmpoint_out;
    NDM::Utils::asci_to_protobuf(ndmpoint, ndmpoint_out);
    NDM::Point p;
    bool       b = p.ParseFromString(ndmpoint_out);

    google::protobuf::util::JsonPrintOptions options;
    options.add_whitespace                = true;
    options.always_print_primitive_fields = true;
    std::string result;
    MessageToJsonString(p, &result, options);
    spdlog::info("{}", result);
    result = "";

    if (!out.empty()) {
        std::ofstream outputfile(out);
        options.add_whitespace = false;
        MessageToJsonString(p, &result, options);
        outputfile << result << std::endl;
        outputfile.close();
        spdlog::info("File '{}' was created.", out);
    }

    return true;
}

} // namespace Utils
} // namespace NDM
