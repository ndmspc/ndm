#include <vector>
#include "ndm.hh"
#include "Point.pb.h"
#include "Space.hh"
#include "Utils.hh"
namespace NDM {
/// constructor
Space::Space()
{ ///
  /// create empty space, to be filled later
  ///
}

Space::~Space()
{
    ///
    /// Destructor
    ///
}

void Space::print() const
{
    ///
    /// Prints the axis space
    ///
    for (auto a : mAxes) {
        a.print();
    }
}

void Space::add(NDM::Axis axis)
{
    ///
    /// Add an axis to the space
    ///
    mAxes.push_back(axis);
}

Axis & Space::axis(int id)
{
    ///
    /// Get axis object at "id" position in space
    ///
    return mAxes.at(id);
}

std::string Space::get_full_path(std::vector<std::string> & paths)
{
    ///
    /// Returns full path correctly edited with slashes and blanks.
    ///
    std::string fullPath, blank = "_";
    int         maxLength = 0;
    for (auto a : paths) {
        if (a.length() > maxLength) {
            maxLength = a.length();
        }
    }
    for (int iPathLevel = 0; iPathLevel < maxLength; iPathLevel++) {
        for (int iPathAxis = 0; iPathAxis < paths.size(); iPathAxis++) {
            paths.at(iPathAxis).resize(maxLength);
            if (!paths.at(iPathAxis).at(iPathLevel)) {
                fullPath += blank;
            }
            else
                fullPath += paths.at(iPathAxis).at(iPathLevel);
            if (iPathAxis == (mAxes.size() - 1)) fullPath += "/";
        }
        if (fullPath.back() != '/') fullPath += "/";
    }
    return std::move(fullPath);
}

void Space::points(std::vector<int> levels, std::vector<NDM::Point> & point, int idAxis)
{
    ///
    /// Fills the passed vector "point" with histogram's minimum, maximum, and path of output file to the console. The
    /// values are decided from the number of axes and levels in space.
    ///
    if (idAxis >= mAxes.size()) {
        spdlog::error("idAxis cannot be more than total number of axes in space!!!");
    }

    if (idAxis == 0 && mTmpMins.size() == 0) {
        spdlog::trace("Space::points -> Init");
        mTmpMins.resize(mAxes.size());
        mTmpPaths.resize(mAxes.size());

        if (levels.size() != mAxes.size()) {
            spdlog::error("Total number of levels must be equal to total number of axes!!!");
            return;
        }

        for (int iAxes = 0; iAxes < mAxes.size(); iAxes++) {
            axis(iAxes).split(mTmpMins.at(iAxes), levels[iAxes]);
            mTmpPoint.add_coordinates();
        }
    }

    NDM::Coordinate * c;
    double            min;
    double            max;
    std::string       path;

    for (auto m : mTmpMins[idAxis]) {
        c = mTmpPoint.mutable_coordinates(idAxis);
        mAxes[idAxis].find(m, min, max, path, levels.at(idAxis));
        spdlog::trace("v[{}][{}] size[{}] min[{}], max[{}], path[{}]", idAxis, m, mTmpMins[idAxis].size(), min, max,
                      path);
        // mAxes[idAxis].print();

        c->set_min(min);
        c->set_max(max);
        c->set_isbin(mAxes[idAxis].is_bin());
        c->set_info(mAxes[idAxis].info());
        mTmpPaths[idAxis] = path;
        if (idAxis < (mAxes.size() - 1)) {
            points(levels, point, idAxis + 1);
        }
        else if (idAxis == (mAxes.size() - 1)) {

            // fill point
            mTmpPoint.set_path(get_full_path(mTmpPaths));
            point.push_back(mTmpPoint);
            // mTmpPoint.PrintDebugString();
        }
        else {
            spdlog::error("Space::points -> idAxis[{}] is higher then number of axis '{}' !!!", idAxis, mAxes.size());
        }
    }
}

void Space::find_point(std::vector<int> & coordinates, std::vector<int> & levels, NDM::Point & point)
{
    ///
    /// Finds point
    ///

    if (mTmpPaths.size() != coordinates.size()) {
        mTmpPaths.resize(coordinates.size());
        mTmpMins.resize(coordinates.size());
    }

    double            min, max;
    NDM::Coordinate * c;
    std::string       path;
    for (int iCoord = 0; iCoord < coordinates.size(); iCoord++) {

        mAxes[iCoord].split(mTmpMins.at(iCoord), levels[iCoord]);
        if (point.coordinates_size() < coordinates.size()) {
            c = point.add_coordinates();
        }
        c = point.mutable_coordinates(iCoord);

        mAxes[iCoord].find(mTmpMins[iCoord][coordinates[iCoord] - 1], min, max, path, levels.at(iCoord));
        spdlog::trace("v[{}][{}] size[{}] min[{}], max[{}], path[{}]", iCoord,
                      mTmpMins[iCoord][coordinates[iCoord] - 1], mTmpMins[iCoord].size(), min, max, path);

        c->set_min(min);
        c->set_max(max);
        c->set_info(mAxes[iCoord].info());
        mTmpPaths[iCoord] = path;
    }
    point.set_path(get_full_path(mTmpPaths));

} // namespace NDM

} // namespace NDM