#include "Config.hh"
#include "Space.hh"
#include <iostream>
namespace NDM {
Config::Config()
{
    ///
    /// Constructor
    ///
}
Config::~Config()
{
    ///
    /// Destructor
    ///
}

bool Config::load(std::string file)
{
    ///
    /// Load config file
    ///

    struct stat buffer;
    bool        rc = (stat(file.c_str(), &buffer) == 0);
    if (!rc) {
        spdlog::error("File '{}' doesn't exists !!! ", file);
        return false;
    }

    mConfig = YAML::LoadFile(file);

    // Clearing previous state
    if (mSpace != nullptr) delete mSpace;
    mLevels.clear();

    mEnvs.clear();

    YAML::Node envsType = mConfig["envs"];
    for (std::size_t i = 0; i < envsType.size(); i++) {
        mEnvs.push_back(
            fmt::format("{}={}", envsType[i]["name"].as<std::string>(), envsType[i]["value"].as<std::string>()));
    }

    mSpace = new Space();

    YAML::Node axes = mConfig["space"]["axes"];
    for (auto ax : axes) {
        if (!ax["name"]) {
            spdlog::warn("Name of axis was not found !!! Skipping axis ...");
            continue;
        }
        if (!ax["min"]) {
            spdlog::warn("Min value in axis was not found !!! Skipping axis [{}] ...", ax["name"].as<std::string>());
            continue;
        }
        if (!ax["max"]) {
            spdlog::warn("Max value in axis was not found !!! Skipping axis [{}] ...", ax["name"].as<std::string>());
            continue;
        }
        if (!ax["level"]) {
            spdlog::warn("Level value in axis was not found !!! Skipping axis [{}] ...", ax["name"].as<std::string>());
            continue;
        }
        NDM::Axis a(ax["min"].as<double>(), ax["max"].as<double>());
        if (ax["isbin"]) a.is_bin(ax["isbin"].as<bool>());
        if (ax["info"]) a.info(ax["info"].as<std::string>());
        mSpace->add(a);
        mLevels.push_back(ax["level"].as<int>());
    }

    return true;
}

void Config::print() const
{
    ///
    /// Prints config file
    ///
    std::cout << mConfig << std::endl;
}

} // namespace NDM
